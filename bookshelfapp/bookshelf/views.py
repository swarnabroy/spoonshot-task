from django.shortcuts import render
import requests
from .forms import BookForm


def home(request):
    if request.method == 'GET':
        search_box = request.GET.get('search_box')
        response = requests.get('https://www.googleapis.com/books/v1/volumes?q=' + str(search_box))
        data = response.json()
        return render(request, 'core/home.html', {'data':data})

def form_view(request):
    if request.method == 'POST':
        form = BookForm(request.POST)
        if form.is_valid():
            form.save() # saves the form if its validated
    else:
        form = BookForm()
    return render(request, 'core/new_book.html', {'form':form} )

# def add_book(request):
#     if request.method == 'GET':
#         search_box = request.GET.get('search_box')
#         response = requests.get('https://www.googleapis.com/books/v1/volumes?q=' + str(search_box))
#         data = response.json()
#         return render(request, 'core/home.html', {'data':data})
