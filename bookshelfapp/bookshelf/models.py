from django.db import models

# Create your models here.

class Books(models.Model):


    choices = (
        ('AVAILABLE', 'Item ready to be purchased'),
        ('SOLD', 'Item already purchased'),
        ('RESTOCKING', 'Item restocking in few days')
    )
    e_id = models.CharField(max_length=100)
    title = models.CharField(max_length=1000)
    status = models.CharField(max_length=10, choices=choices, default='AVAILABLE')
    Author = models.CharField(max_length=100)
    type = models.CharField(max_length=200, blank=False)
    price = models.IntegerField()


    def __str__(self):
        return self.title
